/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.storage.list.getter;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Sylos
 */
public class UserInput
{
    static Scanner scan = new Scanner(System.in);
    public static String getUserInput() 
    {
        Scanner scan = new Scanner(System.in);

        String userInput = scan.nextLine();

        return userInput;
    }
    
    public static void closeScanner()
    {
        scan.close();
    }
}
