/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.storage.list.getter;

/**
 *
 * @author Sylos
 */
public enum UserControlEnum
{
    END ("End"),
    GRAB_IMAGES ("read"),
    ADD_IMAGE ("Add");
    
    private final String command;
    UserControlEnum(String command)
    {
        this.command = command;
    }
    
    public String toString()
    {
        return command;
    }
            
}
