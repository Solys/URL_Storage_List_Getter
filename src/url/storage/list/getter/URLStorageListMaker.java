/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.storage.list.getter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author Sylos
 */
public class URLStorageListMaker
{
    public static void main(String[] args)
    {
        try
        {
            String path = "C:\\Users\\Sylos\\Documents\\Programs\\ProjectTextFiles\\urlgetter\\endoutlineLinks.txt";
            boolean endInput = false;
            FileIO fileIO = new FileIO(path);
            while (!endInput)
            {
                System.out.println("Input Command");
                String userInput = UserInput.getUserInput();
                
                if (userInput.equalsIgnoreCase(UserControlEnum.END.toString()))
                {
                    endInput = true;
                    UserInput.closeScanner();
                    fileIO.closeSystem();
                } else if (userInput.equalsIgnoreCase(UserControlEnum.GRAB_IMAGES.toString()))
                {
                    System.out.println("How many images would you like to grab? ");
                    ArrayList<String> textLines = fileIO.readFromFile();
                    fileIO.printInReverseOrder(textLines);
                } else if (userInput.equalsIgnoreCase(UserControlEnum.ADD_IMAGE.toString()))
                {
                    System.out.println("Add Image");
                    fileIO.writeToFile(UserInput.getUserInput());
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
