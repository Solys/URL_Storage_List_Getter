/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.storage.list.getter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author Sylos
 */
public class FileIO
{

    File file;
    FileWriter fileWriter;
    BufferedWriter bufferedWriter;
    
    FileReader fileReader;
    BufferedReader bufferedReader;
    long lineCount;
    public FileIO(String path) throws IOException
    {
        
        file = new File(path);
        if(file.createNewFile())
            System.out.println("File created");
        System.out.println("File exists.");
        
        fileWriter = new FileWriter(file.getAbsoluteFile(),true);
        bufferedWriter = new BufferedWriter(fileWriter);
        
        
        fileReader = new FileReader(path);
        bufferedReader = new BufferedReader(fileReader);
        
        lineCount = Files.lines(Paths.get(path)).count();
        System.out.println("Number of lines in file: " + lineCount);
    }
    
    public void writeToFile(String input) throws IOException
    {
        System.out.println("adding image");
        bufferedWriter.write(input);
        lineCount++;
        if (lineCount % 5 == 0)
            bufferedWriter.newLine();
        bufferedWriter.newLine();
    }
    
    public ArrayList<String> readFromFile() throws IOException
    {
        String currentLine = "End of file";
        ArrayList<String> lines = new ArrayList();
        
        while((currentLine = bufferedReader.readLine()) != null)
        {
            lines.add(currentLine);
        }
        
        return lines;
    }
    
    
    public void printInReverseOrder (ArrayList<String> lines)
    {
        for(int i = lines.size()-1; i>= 0;i--)
        {
            System.out.println(lines.get(i));
        }
    }
    public void closeSystem() throws IOException
    {
        bufferedWriter.close();
        fileWriter.close();
    }
}
