/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package url.storage.list.getter;

/**
 *
 * @author Sylos
 */
public class URL
{
    private String url;
    
    public URL(String input)
    {
        url = input;
    }
    
    public String getURL()
    {
        return url;
    }
    
    public void setURL(String input)
    {
        url = input;
    }
    
    
}
